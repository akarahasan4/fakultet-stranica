let assert = chai.assert;
var input, filter, tr, td, a, i, txtValue,table;
describe('Pretraga',function() {

	describe('ppredmet()', function(){
		it('Svi predmeti prikazani kad je prazan string unesen',function(){
			
			input= document.getElementById('ppredmet');
			input.value='';
			pPredmet();
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			for (i = 0; i < tr.length; i++) {
   			 td = tr[i].getElementsByTagName("td")[0];
   			 	if(tr[i].style.display ==''){
   			 		brojac++;
   			 	}
			}	
			
			assert.equal(brojac,tr.length,"Ne ispisuje sve elemente");
			
		});

	});

	describe('pnastavnik()', function(){
		it('Nista nije prikazano sa nepostojecim nastavnikom',function(){
			
			input= document.getElementById('pnastavnik');
			input.value='žaba';
			pNastavnik();
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			for (i = 0; i < tr.length; i++) {
   			 td = tr[i].getElementsByTagName("td")[0];
   			 	if(tr[i].style.display ==''){
   			 		brojac++;
   			 	}
			}	
		
			//ovdje je 2 jer ispise PRVA GODINA od tabele i to broji i druga godina
			assert.equal(brojac,2,"Ispisuje elemente koje ne treba");

			
		});

	});

	describe('pnastavnik()', function(){
		it('Predmeti samo od upisanog profesora',function(){
			
			input= document.getElementById('pnastavnik');
			input.value='Željko Jurić';
			filter = input.value.toUpperCase();
			pNastavnik();
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			let brojac2=0;
			for (i = 0; i < tr.length; i++){
    			td = tr[i].getElementsByTagName("td")[2];
    			if(td){
    				txtValue=td.textContent || td.innerText;
    				if(txtValue.toUpperCase()==filter){
    					brojac2++;
    				}
    			if (tr[i].style.display==''){
    				  if(txtValue.toUpperCase()==filter){
    				  	brojac++;
    				  }    			
   			 	}
   			 	
  				}
  			}	
			//ovdje je 1 jer ispise PRVA GODINA od tabele i to broji
			assert.equal(brojac,brojac2,"Ispisuje elemente sa datim profesorom");
			
		
		});

	});

	describe('pgodina()', function(){
		it('Predmeti sa druge godine',function(){
			
			//input= document.getElementById('pgodina');
			//input.value='2';
			
			pGodine(2);
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			let brojac2=0;
			for (i = 0; i < 4; i++){
    			
    			if (tr[i].style.display=='none'){
    				  
    				  	brojac=0;
    				     			
   			 	}else brojac++;
   			 	
  				}
  				
			//poredi se sa 4 jer brojac pocinje od 1 cim naidje na istinitu vrijednost
			assert.equal(brojac,0,"Visak elemenata");
			
		
		});

	});

	describe('pgodina()', function(){
		it('Predmeti sa prve godine',function(){
			
			//input= document.getElementById('pgodina');
			//input.value='2';
			
			pGodine(1);
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			let brojac2=0;
			for (i =5 ; i < 9; i++){
    			
    			if (tr[i].style.display=='none'){
    				  
    				  	brojac=0;
    				     			
   			 	}else brojac++;
   			 	
  				}
  				
			//poredi se sa 4 jer brojac pocinje od 1 cim naidje na istinitu vrijednost
			assert.equal(brojac,0,"Visak elemenata");
			
		
		});

	});

	describe('pgodina()', function(){
		it('Druge godine umjesto prve i druge',function(){
			
			//input= document.getElementById('pgodina');
			//input.value='2';
			
			pGodine(3);
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			let brojac2=0;
			for (i =0 ; i < 9; i++){
    			
    			if (tr[i].style.display=='none'){
    				  
    				  	brojac++;
    				     			
   			 	}else brojac=0;
   			 	
  				}
  				
			//poredi se sa 4 jer brojac pocinje od 1 cim naidje na istinitu vrijednost
			assert.equal(brojac,0,"Ispisuje elemente sa datim profesorom");
			
		
		});

	});

describe('pnastavnik()', function(){
		it('Svi predmeti prikazani kad je prazan string unesen kod nastavnika',function(){
			
			input= document.getElementById('pnastavnik');
			input.value='';
			pNastavnik();
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			for (i = 0; i < tr.length; i++) {
   			 td = tr[i].getElementsByTagName("td")[0];
   			 	if(tr[i].style.display ==''){
   			 		brojac++;
   			 	}
			}	
			
			assert.equal(brojac,tr.length,"Ne ispisuje sve elemente");
			
		});

	});

describe('ppredmet()', function(){
		it('Nista nije prikazano sa nepostojecim predmetom',function(){
			
			input= document.getElementById('ppredmet');
			input.value='žaba';
			pPredmet();
			let brojac=0;
			 table = document.getElementById("tab1");
			  tr = table.getElementsByTagName('tr');
			for (i = 0; i < tr.length; i++) {
   			 td = tr[i].getElementsByTagName("td")[0];
   			 	if(tr[i].style.display ==''){
   			 		brojac++;
   			 	}
			}	
		
			//ovdje je 1 jer ispise PRVA GODINA od tabele i to broji
			assert.equal(brojac,1,"Ispisuje elemente koje ne treba");

			
		});

	});

});
