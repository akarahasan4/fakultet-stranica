


/*povecavanje slike tako sto pokupimo id od profesora i transformisemo sliku 2 puta, 
funkcija transform ne pomjera druge elemente*/
function veca(i){
	var x = document.getElementById('p'+i);
	x.style.transform="scale(2)";


}
/*funkcija vraca sliku u pocetno stanje na isti nacin kako i pvoecavanje samo sto
 se transformise na stare dimenzije jer transform mnozi originalne dimenzije sa brojem koji se unese*/
function manja(i){
	var x = document.getElementById('p'+i);
	x.style.transform="scale(1)";
	
}

/*slijedece funkcije se koriste za sakrivanje predmeta na osnovu pritiska dugmeta
radi pomocu jquerya tako sto dohvatamo klase i mjenjamo im display propertye na none i block tj na sakrivanje i prikazivanje */
//prikazi predmete prve godine
function prikazip(){
	
	
	$('.prva').css('display','block');
	$('.druga').css('display','none');

	

}
//prikazi predmete druge godine
function prikazid(){
	
	$('.prva').css('display','none');
	$('.druga').css('display','block');

}
//prikazi sve predmete
function prikazis(){
	
	$('.prva').css('display','block');
	$('.druga').css('display','block');

}
/*Mjenjanje boje predmeta dobijemo broj profesora sa jqueryem nadjemo klasu i mjenjamo boje*/
//postavlja boju
function predmeti(i){

	$('.p'+i).css('background-color','yellow');
}
//sklanja boju
function npredmeti(i){
	$('.p'+i).css('background-color','transparent');
}